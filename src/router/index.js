import { createWebHistory, createRouter } from 'vue-router'

import Home from '../pages/Home'
import Blog from '../pages/Blog'
import Article from '../pages/Article'
import Works from '../pages/Works'
import About from '../pages/About'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/blog/',
    name: 'Blog',
    component: Blog
  },
  {
    path: '/blog/:slug',
    name: 'Article',
    component: Article
  },
  /* {
    path: '/about/',
    name: 'Article',
    component: Article
  }, */
  {
    path: '/about/',
    name: 'about',
    component: About
  },
  {
    path: '/works/',
    name: 'Works',
    component: Works
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
